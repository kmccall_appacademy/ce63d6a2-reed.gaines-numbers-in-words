class Fixnum

  def in_words

    terms = {
        1000000000000 => "trillion",
        1000000000 => "billion",
        1000000 => "million",
        1000 => "thousand",
        100 => "hundred",
        90 => "ninety",
        80 => "eighty",
        70 => "seventy",
        60 => "sixty",
        50 => "fifty",
        40 => "forty",
        30 => "thirty",
        20 => "twenty",
        19=> "nineteen",
        18=> "eighteen",
        17=> "seventeen",
        16=> "sixteen",
        15=> "fifteen",
        14=> "fourteen",
        13=> "thirteen",
        12=> "twelve",
        11 => "eleven",
        10 => "ten",
        9 => "nine",
        8 => "eight",
        7 => "seven",
        6 => "six",
        5 => "five",
        4 => "four",
        3 => "three",
        2 => "two",
        1 => "one"
      }


    answer = ""
    string = self.to_s.delete("_")
    real_num = string.to_i

    terms.each do |i, s|
      if self == 0
        return "zero"
      elsif self == i && self < 100
        return terms[self]
      elsif self < 100 && self / i >= 1
        return terms[self - self % 10] + " " + terms[self % 10]
      elsif self / i >= 1
        if (self % i).in_words != "zero"
          return (self / i).in_words + " " + terms[i] + " " + (self % i).in_words
        else
          return terms[self / i] + " " + terms[i]
        end
      end
    end
  end
end
